#pragma once
#include "Osoba.h"

using namespace std;

class Klient {
	Osoba osoba;
	string uslugi;
	int numer_klienta:

public:
	Klient();
	Klient(Osoba _osoba, string _uslugi, int _numer_klienta);

	void setOsoba(Osoba);
	void setUslugi(string);
	void setNumer_klienta(int);

	Osoba getOsoba();
	string getUslugi();
	int getNumerKlienta();
};
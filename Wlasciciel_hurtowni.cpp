#include "Wlasciciel_hurtowni.h"

Wlasciciel_hurtowni::Wlasciciel_hurtowni()
{
	cout << "Dziala konstruktor klasy Wlasciciel_hurtowni bez argumentow.\n";
}

Wlasciciel_hurtowni::Wlasciciel_hurtowni(Osoba _osoba, Adres _adres, string _nazwa_hurtowni)
{
	cout << "Dziaka kostruktor klasy Wlasciciel_hurtowni z argumentami.\n";
}

void Wlasciciel_hurtowni::setOsoba(Osoba osoba) { osoba = osoba; }
void Wlasciciel_hurtowni::setAdres(Adres adres) { adres = adres; }
void Wlasciciel_hurtowni::setNazwa_hurtowni(string nazwa_hurtowni) { nazwa_hurtowni = nazwa_hurtowni; }

Osoba Wlasciciel_hurtowni::getOsoba(){return Osoba();}
Adres Wlasciciel_hurtowni::getAdres(){return Adres();}
string Wlasciciel_hurtowni::getNazwa_hurtowni(){return nazwa_hurtowni;}

#include "Osoba.h"

Osoba::Osoba()
{
	cout << "Dziala konstruktor klasy Osoba bez argumentow.\n";
	imie = "Jan";
	nazwisko = "Kowalski";
	Adres;
}

Osoba::Osoba(string _imie, string _nazwisko, Adres _adres)
{
	cout << "Dziala konstruktor klasy Osoba z argumentami.\n";
	imie = _imie;
	nazwisko = _nazwisko;
	Adres;
}

void Osoba::setImie(string imie) { imie = imie; }
void Osoba::setNazwisko(string nazwisko) { nazwisko = nazwisko; }
void Osoba::setAdres(Adres adres) { adres = adres; }

string Osoba::getImie(){return imie;}
string Osoba::getNazwisko(){return nazwisko;}
Adres Osoba::getAdres(){return Adres();}

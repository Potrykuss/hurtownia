#pragma once
#include"klasy_do_Siec_hurtowni.h"

class Siec_hurtowni {
	Hurtownia nazwa;
	Wlasciciel_hurtowni numer;
	Klient id;

public:
	Siec_hurtowni();
	Siec_hurtowni(Hurtownia, Wlasciciel_hurtowni, Klient);

	void setHurtownia(Hurtownia);
	void setWlasciciel_hurtowni(Wlasciciel_hurtowni);
	void setKlient(Klient);

	Hurtownia getHurtownia();
	Wlasciciel_hurtowni getWlasciciel_hurtowni();
	Klient getKlient();

};

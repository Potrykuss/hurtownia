#include "Adres.h"
#include <iostream>

using namespace std;

Adres::Adres()
{
	cout << "Dziala konstruktor klasy Adres bez agrumentow.\n";
	miasto = "Gdynia";
	ulica = "Pierwsza";
	numer_budynku = 1;
	kod_pocztowy1 = 10;
	kod_pocztowy2 = 100;
}

Adres::Adres(string _miasto, string _ulica, int _numer_budynku, int _kod_pocztowy1, int _kod_pocztowy2)
{
	cout << "Dziala konstruktor klasy Adres z argumentami.\n";
}

void Adres::setMiasto(string miasto) { miasto = miasto; }
void Adres::setUlica(string ul) { ulica = ul; }
void Adres::setNumer_budynku(int nr) { numer_budynku = nr; }
void Adres::setKod_pocztowy1(int kod1) { kod_pocztowy1 = kod1; }
void Adres::setKod_pocztowy2(int kod2) { kod_pocztowy2 = kod2; }

string Adres::getMiasto(){return miasto;}
string Adres::getUlica(){return ulica;}
int Adres::getNumer_budynku(){return numer_budynku;}
int Adres::getKod_pocztowy1(){return kod_pocztowy1;}
int Adres::getKod_pocztowy2(){return kod_pocztowy2;}

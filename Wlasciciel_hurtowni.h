#pragma once
#include<iostream>
#include "Osoba.h"
#include "Adres.h"

using namespace std;

class Wlasciciel_hurtowni {
	Osoba osoba;
	Adres adres;
	string nazwa_hurtowni;

public:
	Wlasciciel_hurtowni();
	Wlasciciel_hurtowni(Osoba _osoba, Adres _adres, string _nazwa_hurtowni);

	void setOsoba(Osoba);
	void setAdres(Adres);
	void setNazwa_hurtowni(string);

	Osoba getOsoba();
	Adres getAdres();
	string getNazwa_hurtowni();

};


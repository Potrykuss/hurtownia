#include "Hurtownia.h"

Hurtownia::Hurtownia()
{
	cout << "Dziala konstruktor klasy Hurtownia bez argumentow.\n";
}

Hurtownia::Hurtownia(string _nazwa_hurtowni, string _materialy, Adres _adres, Wlasciciel_hurtowni _wlasciciel_hurtowni)
{
	cout << "Dziala konstruktor klasy Hurtownia z argumentami.\n";
}

void Hurtownia::setNazwa_hurtowni(string nazwa_hurtowni) { nazwa_hurtowni = nazwa_hurtowni; }
void Hurtownia::setMaterialy(string materialy) { materialy = materialy; }
void Hurtownia::setAdres(Adres adres) { adres = adres; }
void Hurtownia::serWlasciciel_hurtowni(Wlasciciel_hurtowni wlasciciel_hurtowni) { wlasciciel_hurtowni = wlasciciel_hurtowni; }

string Hurtownia::getNazwa_hurtowni(){return nazwa_hurtowni;}
string Hurtownia::getMaterialy(){return materialy;}
Adres Hurtownia::getAdres(){return Adres();}
Wlasciciel_hurtowni Hurtownia::getWlasciciel_hurtowni(){return Wlasciciel_hurtowni();}

#pragma once
#include <iostream>
#include "Adres.h"
#include "Klient.h"
#include "Wlasciciel_hurtowni.h"

using namespace std;

class Hurtownia {
	string nazwa_hurtowni;
	string materialy;
	Adres adres;
	Wlasciciel_hurtowni wlasciciel_hurtowni;

public:
	Hurtownia();
	Hurtownia(string _nazwa_hurtowni, string _materialy, Adres _adres, Wlasciciel_hurtowni _wlasciciel_hurtowni);

	void setNazwa_hurtowni(string);
	void setMaterialy(string);
	void setAdres(Adres);
	void serWlasciciel_hurtowni(Wlasciciel_hurtowni);

	string getNazwa_hurtowni();
	string getMaterialy();
	Adres getAdres();
	Wlasciciel_hurtowni getWlasciciel_hurtowni();
};
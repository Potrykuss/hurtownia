#include "Siec_hurtowni.h"

Siec_hurtowni::Siec_hurtowni()
{
	cout << "Dziala konstruktor klasy Siec_hurtowni bez argumentow.\n";
}

Siec_hurtowni::Siec_hurtowni(Hurtownia, Wlasciciel_hurtowni, Klient)
{
	cout << "Dziala konstruktor klasy Siec_hurtowni z argumentami.\n";
}

void Siec_hurtowni::setHurtownia(Hurtownia _nazwa) { nazwa = _nazwa; }
void Siec_hurtowni::setWlasciciel_hurtowni(Wlasciciel_hurtowni _numer) { numer = _numer; }
void Siec_hurtowni::setKlient(Klient _id) { id = _id; }

Hurtownia Siec_hurtowni::getHurtownia(){return nazwa;}
Wlasciciel_hurtowni Siec_hurtowni::getWlasciciel_hurtowni(){return numer;}
Klient Siec_hurtowni::getKlient(){return id;}

#pragma once
#include <iostream>

using namespace std;

class Adres {

	string miasto;
	string ulica;
	int numer_budynku;
	int kod_pocztowy1;
	int kod_pocztowy2;

public:
	Adres();
	Adres(string _miasto, string _ulica, int _numer_budynku, int _kod_pocztowy1, int _kod_pocztowy2);

	void setMiasto(string);
	void setUlica(string);
	void setNumer_budynku(int);
	void setKod_pocztowy1(int);
	void setKod_pocztowy2(int);

	string getMiasto();
	string getUlica();
	int getNumer_budynku();
	int getKod_pocztowy1();
	int getKod_pocztowy2();
};

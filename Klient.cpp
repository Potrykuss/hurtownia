#include "Klient.h"

Klient::Klient()
{
	cout << "Dziala konstruktor klasy Klient bez argumentow.\n";
}

Klient::Klient(Osoba _osoba, string _uslugi, int _numer_klienta)
{
	cout << "Dziala kostruktor klasy Klient z argumentami.\n";
}

void Klient::setOsoba(Osoba osoba) { osoba = osoba; }
void Klient::setUslugi(string uslugi) { uslugi = uslugi; }
void Klient::setNumer_klienta(int nk) { numer_klienta = nk; }

Osoba Klient::getOsoba(){return Osoba();}
string Klient::getUslugi(){return uslugi;}
int Klient::getNumerKlienta(){return numer_klienta;}

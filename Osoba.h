#pragma once
#include <iostream>
#include "Adres.h"

using namespace std;

class Osoba {
	string imie;
	string nazwisko;
	Adres adres;

public:
	Osoba();
	Osoba(string _imie, string _nazwisko, Adres _adres);

	void setImie(string);
	void setNazwisko(string);
	void setAdres(Adres);

	string getImie();
	string getNazwisko();
	Adres getAdres();
};